//
//  ViewController.m
//  SEProject
//
//  Created by JamesRoyal on 1/10/14.
//  Copyright (c) 2014 James Royal. All rights reserved.
//

#import "ViewController.h"
#import <Accounts/Accounts.h>
#import <Social/Social.h>
#import "TwitterUser.h"
#import "ProfileViewController.h"
#import "TwitterCell.h"

#import "UIImageView+UIActivityIndicatorForSDWebImage.h"

@interface ViewController ()
@property(strong)NSArray *data;
@property(strong)NSMutableArray * users;
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Twitter user search";
    //[self searchTwitter];
    [_searchBar setText:@"Adknowledge"];
    //send of the first search
    [self searchTwitter:_searchBar.text];
    
    if (_tableData == nil){
        _tableData = [[NSMutableArray alloc]init];
    }
        // Do any additional setup after loading the view, typically from a nib.
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [self searchTwitter:searchBar.text];
    [_searchBar resignFirstResponder];
}
/**
 *  This funtion handles the twitter query search.
 *  It authenticates with the twitter api using the
 *  twitter account located on the device
 */

- (void)searchTwitter:(NSString*)query{
    
    //Accessing the local store of all accounts located on the device
    ACAccountStore * aStore = [[ACAccountStore alloc]init];
    //Identifiying that we want the specific twitter account;
    ACAccountType * twitterAccountType = [aStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    
    //Requesting access to the users local twitter account located on the device
    [aStore requestAccessToAccountsWithType:twitterAccountType options:nil completion:^(BOOL granted, NSError *error) {
        
        if (granted == YES) {
            //We successfully gained access to the twitter account
            //may be more than one twitter account on the phone
            NSArray *accounts = [aStore accountsWithAccountType:twitterAccountType];
            
            if([accounts count] > 0){
                //we have at least one account
                ACAccount * twitterAccount = [accounts lastObject];
                // This url represents the desired search url endpoint
                NSURL *searchURL = [NSURL URLWithString:@"https://api.twitter.com/1.1/users/search.json"];
                
                NSMutableDictionary * searchParams = [[NSMutableDictionary alloc]init];
                //setting the search paramater
                [searchParams setObject:query forKey:@"q"];
                [searchParams setObject:@"10" forKey:@"count"];
                [searchParams setObject:@"1" forKey:@"include_entities"];
                //This parameter was not listed under the 1.1 version of twitters api
                //[searchParams setObject:@"mixed" forKey:@"result_type"];
                [searchParams setObject:@"5" forKey:@"per_page"];
                
                
                SLRequest * twitterRequest = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:SLRequestMethodGET URL:searchURL parameters:searchParams];
                
                
                twitterRequest.account = twitterAccount;
                
                [twitterRequest performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
                    
                    if (error) {
                        NSLog(@"error parsing data");
                        return;
                    }
                   
                    _data = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves error:&error];
                    
                    NSLog(@"%@", _data);
                    
                    NSMutableArray * users = [[NSMutableArray alloc]init];
                 
                    for (int i = 0; i < [_data count]; i++) {
                     
                        NSDictionary *dict = [_data objectAtIndex:i];
                        
                        TwitterUser *tempUser = [[TwitterUser alloc]initWithDict:dict];
                        
                        [users addObject:tempUser];
                        
                       /*
                        for (id key in _data) {
                            NSLog(@"key: %@, value: %@ \n", key, [dict objectForKey:key]);
                        }
                        */
                    }
                    
                    //Apply an alphabetical sort to the returned results
                    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
                    [users sortUsingDescriptors:[NSArray arrayWithObject:sort]];
                    
                    _tableData = users;
                    
                    //Tell the Table view to reload our data
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [_tableView reloadData];
                    });
                    
                    
                }];
                
                
            }
            else{
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Sorry there is no Twitter account stored on this device. Please quit the application and add your Twitter account to the device through the settings application! Then relaunch" delegate:Nil cancelButtonTitle:Nil otherButtonTitles:@"OK", nil];
                    [alert show];
                });
            }
            
        }
        else {
            //there is a problem
            
            NSLog(@"%@", [error localizedDescription]);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                //there are no twitter accounts located on the phone
                
                // Fail gracefully...
                NSLog(@"%@",error.description);
                
                if([error code]== ACErrorAccountNotFound){
                    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Sorry there is no Twitter account stored on this device. Please quit the application and add your Twitter account to the device through the settings application! Then relaunch" delegate:Nil cancelButtonTitle:Nil otherButtonTitles:@"OK", nil];
                    [alert show];
                }
                else{
                    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Twitter account access denied" delegate:Nil cancelButtonTitle:Nil otherButtonTitles:@"OK", nil];
                    [alert show];
                }
                
                
            });
            
            
            
        }
        
    }];
    
}

#pragma mark UITableView Delegate Methods
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 66;
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
	return 1;
}

//How many rows in our table view
- (NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)section{

	return [_tableData count];  //numRows;
}


- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
        static NSString *CellIdentifier = @"TwitterCell";
    
        TwitterCell *cell = nil;
    
        cell = [_tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
        //Not needed due to storyboards
        //if (cell == nil)
        //    {
        //        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        //   }
    
        if ([[_tableData objectAtIndex:indexPath.row]isKindOfClass:[TwitterUser class]]) {
        
            TwitterUser * user = [_tableData objectAtIndex:indexPath.row];
        
            cell.name.text = user.name;
        
            [cell.profilePic setImageWithURL:user.profilePicUrL
                       placeholderImage:[UIImage imageNamed:@"loading.gif"] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    
        }
        else{
            cell.name.text = @"No Users Found!";
        }
    
    return cell;
}

//Called when a user selects a table view cell
- (void)tableView:(UITableView *)aTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
  
    //Deselect the row in our table view
    [_tableView deselectRowAtIndexPath:indexPath animated:YES];
 
}

//handles the transition between the two views
//When a user selects a row in our tableview
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showProfileViewController"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        ProfileViewController *destViewController = segue.destinationViewController;
        destViewController.selectedUser = [_tableData objectAtIndex:indexPath.row];
        //[destViewController setupWithUser];
    }
}






@end
