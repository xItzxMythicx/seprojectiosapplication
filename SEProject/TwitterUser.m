//
//  TwitterUser.m
//  SEProject
//
//  Created by JamesRoyal on 1/10/14.
//  Copyright (c) 2014 James Royal. All rights reserved.
//

#import "TwitterUser.h"

@implementation TwitterUser

//Constructor to initalis the object with a dictionary;
-(id)initWithDict:(NSDictionary*)userDict{
    
    self = [super init];
    
    if (self) {
        
        //Set the id of the user otherwise nil
        if ([userDict objectForKey:@"id"] != [NSNull null]) {
            _userId = [userDict objectForKey:@"id"];
            
        }
        else{
            _userId = nil;
            
        }
        
        //Set the name of the user otherwise nil
        if ([userDict objectForKey:@"name"] != [NSNull null]) {
            _name = [userDict objectForKey:@"name"];
            
        }
        else{
            _name = nil;
            
        }
        
        //Set the username of the user otherwise nil
        if ([userDict objectForKey:@"screen_name"] != [NSNull null]) {
            _screenName = [userDict objectForKey:@"screen_name"];
            
        }
        else{
            _screenName = nil;
            
        }
        
        //Set the number of friends of this user otherwise nil
        if ([userDict objectForKey:@"friends_count"] != [NSNull null]) {
            _numberOfFriends = [userDict objectForKey:@"friends_count"];
            
        }
        else{
            _numberOfFriends = nil;
            
        }
        
        //Set the number of followers for this user otherwise nil
        if ([userDict objectForKey:@"followers_count"] != [NSNull null]) {
            _numberOfFollowers = [userDict objectForKey:@"followers_count"];
            
        }
        else{
            _numberOfFollowers = nil;
            
        }
        //Set the url to this users profile image otherwise nil
        if ([userDict objectForKey:@"profile_image_url"] != [NSNull null]) {
            _profilePicUrL = [NSURL URLWithString:[userDict objectForKey:@"profile_image_url"]];
            
        }
        else{
            _profilePicUrL = nil;
            
        }
        
    }
    
    
    return self;
}

//This function returns an instance of the object in the from of a dictionary
//Its main purpose is so we can post an instance of the object to the webserver
-(NSMutableDictionary*)dictionaryRepresentation{
    //Create a dictionsary that represents this object so we cand post to the webservice
    NSMutableDictionary * dict = [[NSMutableDictionary alloc]init];
    [dict setObject:_name forKey:@"name"];
    [dict setObject:_userId forKey:@"userId"];
    [dict setObject:_screenName forKey:@"screenName"];
    [dict setObject:[_profilePicUrL absoluteString]  forKey:@"profilePicUrl"];
    [dict setObject:_numberOfFriends forKey:@"numberOfFriends"];
    [dict setObject:_numberOfFollowers forKey:@"numberOfFollowers"];
    
    return dict;
}

@end;
