//
//  ViewController.h
//  SEProject
//
//  This class represnets the first view controller wHen the application starts up
//
//  Created by JamesRoyal on 1/10/14.
//  Copyright (c) 2014 James Royal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TwitterUser.h"

@interface ViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>

@property(weak, nonatomic)IBOutlet UITableView *tableView;      //IBOutlet for our table view
@property(weak, nonatomic)IBOutlet UISearchBar *searchBar;
@property(strong)NSArray *tableData;                            //An array to hold table data

/**
 *  This funtion handles the twitter query search.
 *  It authenticates with the twitter api using the
 *  twitter account located on the device
 */
- (void)searchTwitter:(NSString*)query;


@end
