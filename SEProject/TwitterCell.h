//
//  TwitterCell.h
//  SEProject
//
//  This class is a custom table cell for use with our table view
//
//  Created by JamesRoyal on 1/10/14.
//  Copyright (c) 2014 James Royal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TwitterCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *profilePic;   //Image View to display the profile pic of the twitter user
@property (weak, nonatomic) IBOutlet UILabel *name;             //a lable to display the name of the twitter user

@end
