//
//  ProfileViewController.m
//  SEProject
//
//  Created by JamesRoyal on 1/10/14.
//  Copyright (c) 2014 James Royal. All rights reserved.
//

#import "ProfileViewController.h"
#import "TwitterUser.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"

@interface ProfileViewController ()

@end

@implementation ProfileViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder{
    self = [super initWithCoder:decoder];
    if (self){
        [self setTitle:@"Profile View"];
    }
    return self;
}

/**
 *  This function allows for custom setup of the profile view
 *  It is required so that after the segue has taken place we
 *  can populate the view with the appropriate data
 */
-(void)setupWithUser{
    
    [_profilePicIV setImageWithURL:_selectedUser.profilePicUrL
        placeholderImage:[UIImage imageNamed:@"loading.gif"]usingActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    
    [_nameL setText:_selectedUser.name];
    [_screenNameL setText:_selectedUser.screenName];
    [_numOfFollowersL setText:[_selectedUser.numberOfFollowers stringValue]];
    [_numOfFriendsL setText:[_selectedUser.numberOfFriends stringValue]];
  
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupWithUser];
    
    //Notify our webserver of the event letting it know the user
    //selected to view this specific profile
    [self notifyWebServerWithUser:_selectedUser];
	// Do any additional setup after loading the view.
}

/**
 *  This function handles the communication with the webserver
 *  It simply sends a blind url request and does not verify that the
 *  request made it to its destanation. For demonstration purpose only
 *
 *  @param user : The instance of the specific user to be submitted to the webserver for tracking
 */

-(void)notifyWebServerWithUser:(TwitterUser*)user{
    
    NSString * url = @"http://54.203.247.197/";                   //Production server
    //NSString * url = @"http://localhost:3000";                    //Testing server
    //NSString * url = @"http://seproject.herokuapp.com";           //Heroku
    
    NSURL *notifyUrl = [[NSURL alloc]initWithString:url];
    NSMutableURLRequest * notifyRequest = [[NSMutableURLRequest alloc]initWithURL:notifyUrl];
    
    [notifyRequest setHTTPMethod:@"POST"];
    [notifyRequest setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [notifyRequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:[user dictionaryRepresentation]
                                                       options:NSJSONWritingPrettyPrinted error:nil];
    
    
    [notifyRequest setHTTPBody:jsonData];
    
    [NSURLConnection sendAsynchronousRequest:notifyRequest queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
        if (error){
            NSLog(@"Problems when updating site");
        }
        else{
            
            NSLog(@"Updated Site");
        }
    }];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
