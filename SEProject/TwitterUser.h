//
//  TwitterUser.h
//  SEProject
//
//  This class represents a single twitter user that is returned form the webserver apon a successfull search.
//  It allows us to modal the data returned and allows us to send an instance to our webserver for tracking user
//  clicks.
//
//  Created by JamesRoyal on 1/10/14.
//  Copyright (c) 2014 James Royal. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TwitterUser : NSObject

@property(strong)NSNumber *userId;                      //holds the Id of the twitter user stored in the database
@property(strong)NSString *name;                        //holds this users name
@property(strong)NSString *screenName;                  //holds this users screenName
@property(strong)NSNumber *numberOfFriends;             //holds the count of friends this users has
@property(strong)NSNumber *numberOfFollowers;           //holds the count of followers this user has
@property(strong)NSURL *profilePicUrL;                  //holds the url that points to this users profile image


//Constructor to initalis the object with a dictionary;
-(id)initWithDict:(NSDictionary*)userDict;

//This function returns an instance of the object in the from of a dictionary
//Its main purpose is so we can post an instance of the object to the webserver
-(NSMutableDictionary*)dictionaryRepresentation;

@end
