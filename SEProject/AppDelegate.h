//
//  AppDelegate.h
//  SEProject
//
//  Created by JamesRoyal on 1/10/14.
//  Copyright (c) 2014 James Royal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
