//
//  ProfileViewController.h
//  SEProject
//
//  This class represents the profile view of the user
//  When this view is view we need to notify the webserver
//
//  Created by JamesRoyal on 1/10/14.
//  Copyright (c) 2014 James Royal. All rights reserved.
//

#import <UIKit/UIKit.h>
@class TwitterUser;
@interface ProfileViewController : UIViewController


@property(weak)IBOutlet UIImageView *profilePicIV;
@property(weak)IBOutlet UILabel *nameL;
@property(weak)IBOutlet UILabel *screenNameL;
@property(weak)IBOutlet UILabel *numOfFollowersL;
@property(weak)IBOutlet UILabel *numOfFriendsL;

@property(strong)TwitterUser *selectedUser;

/**
 *  This function allows for custom setup of the profile view
 *  It is required so that after the segue has taken place we 
 *  can populate the view with the appropriate data
 */
-(void)setupWithUser;


/**
 *  This function handles the communication with the webserver
 *  It simply sends a blind url request and does not verify that the
 *  request made it to its destanation. For demonstration purpose only
 *
 *  @param user : The instance of the specific user to be submitted to the webserver for tracking
 */
- (void)notifyWebServerWithUser:(TwitterUser*)user;


@end
